<?php

/**
 * Add again em_validation_sq for single text question
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2023 Denis Chenu <http://www.sondages.pro>
 * @license GPL v3
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class TextLogicValidation extends PluginBase
{
    /** @inheritdoc */
    protected static $name = 'TextLogicValidation';
    /** @inheritdoc */
    protected static $description = 'Add text validation equation to 5.X and up.';

    /** @inheritdoc */
    public function init()
    {
        if (intval(App()->getConfig('versionnumber')) <= 3) {
            return;
        }
        $this->subscribe('newQuestionAttributes', 'addTextLogicValidationAttribute');
    }

    /**
    * Add the attribute
    * @see event newQuestionAttributes https://manual.limesurvey.org/NewQuestionAttributes
    **/
    public function addTextLogicValidationAttribute()
    {
        $textLogicValidationAttributes = array(
            'em_validation_sq' => array(
                'name' => 'em_validation_sq',
                'types' => "STUN",
                'category' => 'Logic', // 5.X translate after
                'sortorder' => 220,
                'inputtype' => 'textarea',
                'expression' => 2,
                'xssfilter' => 0,
                'help' => $this->gT('Enter a boolean equation to validate text.'),
                'caption' => $this->gT('Text validation equation')
            ),
            'em_validation_sq_tip' => array(
                'name' => 'em_validation_sq_tip',
                'types' => "STUN",
                'category' => 'Logic', // 5.X translate after
                'sortorder' => 230,
                'inputtype' => 'textarea',
                'expression' => 1,
                'i18n' => true,
                'xssfilter' => 1,
                'help' => $this->gT('This is a tip shown to the participant describing the text validation equation.'),
                'caption' => $this->gT('Text validation tip')
            ),
        );
        $this->getEvent()->append('questionAttributes', $textLogicValidationAttributes);
    }

    /**
    * @inheritdoc
    * Usage of unescaped by default
    */
    public function gT($sToTranslate, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        return parent::gT($sToTranslate, $sEscapeMode, $sLanguage);
    }
}
