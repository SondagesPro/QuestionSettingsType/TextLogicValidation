# TextLogicValidation : Add em_validation_sq to single text quetsion again #

Between LimeSurvey version 3 and 5 : em_validation_sq attribute (Subquestion validation equation) was removed. 

If you use it in your survey : it's deleted when you import or copy a survey, or when you edit the question. This plugin add it again on 5.X version.

Another reason to use it, it's to have 2 different tips if you have 2 different validation.

## Home page & Copyright
- HomePage <http://www.sondages.pro/>
- Copyright ©2023 Denis Chenu <http://sondages.pro>
- [Support](https://support.sondages.pro)
- [Donate](https://support.sondages.pro/open.php?topicId=12)
- [Donate on Liberapay](https://liberapay.com/SondagesPro/)
- [Donate on Open Collective](https://opencollective.com/sondagespro)
- Licence : GNU General Public License <https://www.gnu.org/licenses/gpl-3.0.html>
